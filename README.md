# Requirement
- Go Latest Version
- Minio

# Installation
- clone this repository
- change to workdir
- go mod tidy
- go mod install
- go run .