package main

import (
	"context"
	"flag"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"github.com/mdp/qrterminal/v3"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"google.golang.org/protobuf/proto"
	"mime"
	"os"
	"os/signal"
	"syscall"

	"go.mau.fi/whatsmeow"
	waBinary "go.mau.fi/whatsmeow/binary"
	"go.mau.fi/whatsmeow/store"
	"go.mau.fi/whatsmeow/store/sqlstore"
	"go.mau.fi/whatsmeow/types/events"
	waLog "go.mau.fi/whatsmeow/util/log"
)

var client *whatsmeow.Client
var log waLog.Logger

var logLevel = "INFO"
var debugLogs = flag.Bool("debug", false, "Enable debug logs?")
var requestFullSync = flag.Bool("request-full-sync", false, "Request full (1 year) history sync when logging in?")

func WAConnect() (*whatsmeow.Client, error) {
	waBinary.IndentXML = true
	flag.Parse()

	if *debugLogs {
		logLevel = "DEBUG"
	}
	if *requestFullSync {
		store.DeviceProps.RequireFullSync = proto.Bool(true)
	}
	log = waLog.Stdout("Main", logLevel, true)

	dbLog := waLog.Stdout("Database", "DEBUG", true)
	// Make sure you add appropriate DB connector imports, e.g. github.com/mattn/go-sqlite3 for SQLite
	container, err := sqlstore.New("sqlite3", "file:my_whatsapp.db?_foreign_keys=on", dbLog)
	if err != nil {
		return nil, err
	}
	// If you want multiple sessions, remember their JIDs and use .GetDevice(jid) or .GetAllDevices() instead.
	deviceStore, err := container.GetFirstDevice()
	if err != nil {
		panic(err)
	}
	clientLog := waLog.Stdout("Client", "INFO", true)
	client = whatsmeow.NewClient(deviceStore, clientLog)
	client.AddEventHandler(eventHandler)

	if client.Store.ID == nil {
		// No ID stored, new login
		qrChan, _ := client.GetQRChannel(context.Background())
		err = client.Connect()
		if err != nil {
			return nil, err
		}
		for evt := range qrChan {
			if evt.Event == "code" {
				// Render the QR code here
				// e.g. qrterminal.GenerateHalfBlock(evt.Code, qrterminal.L, os.Stdout)
				// or just manually `echo 2@... | qrencode -t ansiutf8` in a terminal
				//fmt.Println("QR code:", evt.Code)
				qrterminal.GenerateHalfBlock(evt.Code, qrterminal.M, os.Stdout)
			} else {
				fmt.Println("Login event:", evt.Event)
			}
		}
	} else {
		// Already logged in, just connect
		err = client.Connect()
		if err != nil {
			return nil, err
		}
	}
	return client, nil
}

func eventHandler(evt interface{}) {
	switch v := evt.(type) {
	case *events.Message:
		if v.Info.IsGroup && !v.Info.IsFromMe {
			//Get Meta Data Group and Participant
			//resp, err := client.GetGroupInfo(v.Info.Chat)
			//if err == nil {
			//	log.Infof("Group ID: %s", resp.JID)
			//	log.Infof("Group Name: %s", resp.Name)
			//	for _, member := range resp.Participants {
			//		var jids []types.JID
			//		jids = append(jids, member.JID)
			//		participant, err := client.GetUserInfo(jids)
			//		if err == nil {
			//			for _, userParticipant := range participant {
			//				log.Infof("Name: %s", userParticipant.VerifiedName)
			//				log.Infof("Admin: %s", member.IsAdmin)
			//				log.Infof("Status: %s", userParticipant.Status)
			//				log.Infof("Pic: %s", userParticipant.PictureID)
			//				imageUsr, err := client.GetProfilePictureInfo(member.JID, &whatsmeow.GetProfilePictureParams{})
			//				if err == nil {
			//					log.Infof("URL User Picture: %s", imageUsr.URL)
			//				}
			//			}
			//		}
			//	}
			//}

			log.Infof("Number Sender: %s", v.Info.Sender.User) //number
			log.Infof("Name Sender: %s", v.Info.PushName)      //number
			log.Infof("Type Message: %s", v.Info.Type)
			log.Infof("Category Message: %s", v.Info.Category)
			log.Infof("Time Message: %s", v.Info.Timestamp.Unix())

			//Link handling
			linkMessage := v.Message.ExtendedTextMessage
			if linkMessage != nil {
				log.Infof("Text: %s", linkMessage.GetText())
				log.Infof("Link: %s", linkMessage.GetMatchedText())
				thumbLink := linkMessage.GetJpegThumbnail()
				if thumbLink != nil {
					path := fmt.Sprintf("%s%s", v.Info.ID, ".jpeg")
					mimeType := "jpeg"
					err := os.WriteFile(path, thumbLink, 0600)
					if err != nil {
						log.Errorf("Failed to save image: %v", err)
						return
					}
					pushData, errPush := minioPush(&path, &path, &mimeType)
					if errPush == nil {
						log.Infof(pushData.ETag)
						e := os.Remove(path)
						if e != nil {
							panic(e)
						}
					}
					log.Infof("Saved image in message to %s", path)
				}
			}

			//Image Handling
			imgMessage := v.Message.GetImageMessage()
			if imgMessage != nil {
				data, err := client.Download(imgMessage)
				if err != nil {
					log.Errorf("Failed to download image: %v", err)
					return
				}
				exts, _ := mime.ExtensionsByType(imgMessage.GetMimetype())
				path := fmt.Sprintf("%s%s", v.Info.ID, exts[0])
				err = os.WriteFile(path, data, 0600)
				if err != nil {
					log.Errorf("Failed to save image: %v", err)
					return
				}
				pushData, errPush := minioPush(&path, &path, &exts[0])
				if errPush == nil {
					log.Infof(pushData.ETag)
					e := os.Remove(path)
					if e != nil {
						panic(e)
					}
				}
				log.Infof("Saved image in message to %s", path)
			}

			//Stiker Handling
			stickerMessage := v.Message.GetStickerMessage()
			if stickerMessage != nil {
				data, err := client.Download(stickerMessage)
				if err != nil {
					log.Errorf("Failed to download image: %v", err)
					return
				}
				exts, _ := mime.ExtensionsByType(stickerMessage.GetMimetype())
				path := fmt.Sprintf("%s%s", v.Info.ID, exts[0])
				err = os.WriteFile(path, data, 0600)
				if err != nil {
					log.Errorf("Failed to stiker image: %v", err)
					return
				}
				pushData, errPush := minioPush(&path, &path, &exts[0])
				if errPush == nil {
					log.Infof(pushData.ETag)
					e := os.Remove(path)
					if e != nil {
						panic(e)
					}
				}
				log.Infof("Saved stiker in message to %s", path)
			}

			//Document Handling
			docMessage := v.Message.GetDocumentMessage()
			if docMessage != nil {
				data, err := client.Download(docMessage)
				if err != nil {
					log.Errorf("Failed to download image: %v", err)
					return
				}
				exts, _ := mime.ExtensionsByType(docMessage.GetMimetype())
				path := fmt.Sprintf("%s%s", v.Info.ID, exts[0])
				err = os.WriteFile(path, data, 0600)
				if err != nil {
					log.Errorf("Failed to document image: %v", err)
					return
				}
				pushData, errPush := minioPush(&path, &path, &exts[0])
				if errPush == nil {
					log.Infof(pushData.ETag)
					e := os.Remove(path)
					if e != nil {
						panic(e)
					}
				}
				log.Infof("Saved Document in message to %s", path)
			}

			//Audio Handling
			audioMessage := v.Message.GetAudioMessage()
			if audioMessage != nil {
				data, err := client.Download(audioMessage)
				if err != nil {
					log.Errorf("Failed to download image: %v", err)
					return
				}
				exts, _ := mime.ExtensionsByType(audioMessage.GetMimetype())
				path := fmt.Sprintf("%s%s", v.Info.ID, exts[0])
				err = os.WriteFile(path, data, 0600)
				if err != nil {
					log.Errorf("Failed to audio image: %v", err)
					return
				}
				pushData, errPush := minioPush(&path, &path, &exts[0])
				if errPush == nil {
					log.Infof(pushData.ETag)
					e := os.Remove(path)
					if e != nil {
						panic(e)
					}
				}
				log.Infof("Saved Audio in message to %s", path)
			}

			//Video Handling
			videoMessage := v.Message.GetVideoMessage()
			if videoMessage != nil {
				data, err := client.Download(videoMessage)
				if err != nil {
					log.Errorf("Failed to download image: %v", err)
					return
				}
				exts, _ := mime.ExtensionsByType(videoMessage.GetMimetype())
				path := fmt.Sprintf("%s%s", v.Info.ID, exts[0])
				err = os.WriteFile(path, data, 0600)
				if err != nil {
					log.Errorf("Failed to video image: %v", err)
					return
				}
				pushData, errPush := minioPush(&path, &path, &exts[0])
				if errPush == nil {
					log.Infof(pushData.ETag)
					e := os.Remove(path)
					if e != nil {
						panic(e)
					}
				}
				log.Infof("Saved Video in message to %s", path)
			}
		}
	}
}

func minioPush(pathFile *string, objFile *string, mimeType *string) (minio.UploadInfo, error) {
	ctx := context.Background()
	endpoint := "localhost:9000"
	accessKeyID := "minioadmin"
	secretAccessKey := "minioadmin"

	// Initialize minio client object.
	minioClient, err := minio.New(endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(accessKeyID, secretAccessKey, ""),
		Secure: false,
	})
	if err != nil {
		panic(err)
	}

	// Upload the zip file with FPutObject
	info, err := minioClient.FPutObject(ctx, "medias", *objFile, *pathFile, minio.PutObjectOptions{ContentType: *mimeType})
	if err != nil {
		panic(err)
	}

	return info, nil
}

func main() {
	wac, err := WAConnect()
	if err != nil {
		panic(err)
		return
	}

	// Listen to Ctrl+C (you can also do something else that prevents the program from exiting)
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	<-c

	wac.Disconnect()
}
